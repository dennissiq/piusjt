<?php

namespace App\Http\Controllers;

use App\Transferencia;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use stdClass;
use Carbon\Carbon;
use App\TokenAccess;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class TransferenciaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        return view('painel.transferencia.create');
    }

    // Classe para fornecer as informacoes do receptor da transferencia

    public function authenticate(Request $request, stdClass $erro)
    {


       $token = TokenAccess::where('token', $request->token_transacao)->first();
    //    dd($request);

        $input = $request->all();

        
             $request->validate([
            'token_transacao' => 'required|min:4|max:4'
        ]);  
       


        if ( empty($token) ) {

            $request->session()->flash('tipo', 'danger');
            $request->session()->flash('erro', 'O Token digitado é inválido');

            return back()->withInput(); 
        }

        if ($token->estado == 0) {
            $request->session()->flash('tipo', 'danger');
            $request->session()->flash('erro', 'O Token digitado já foi utilizado');

            return back()->withInput();
        }


     
        
        if($token->tipo != 1) {
            return back()->with([
          Session::flash('erro',  'O Token digitado não é válido para este tipo de operação'),
          Session::flash('tipo', 'danger')
        // return 1;
      ]);
      
        }

        // testa agora

        if ($this->validaTempoToken($token)) {
            $request->session()->flash('tipo', 'danger');
            $request->session()->flash('erro', 'O Token digitado expirou');
            
            return back()->withInput();

        }

    

       $token->estado = 0;
       $token->save();

        return redirect()->route('TransferenciaStore');
        // return store();
        // return 1;
    



    }

    public function verificaReceptor(Request $request)
    {
        // linha apenas para teste
        // $request->cpf = 1234;
        // fim linha de teste

        $user_receptor_id = User::where([
            'conta' => $request->conta,
            'agencia' => $request->agencia
        ])
            ->select('id', 'cpf', 'name', 'agencia', 'conta')
            ->first();

        // Caso o usuario não exista
        if (! $user_receptor_id)
            return response()->json(['status' => 0, 'message' => 'Dados inválidos']);

        return response()->json($user_receptor_id);

        // $user = authenticate()
    }

    public function store(Request $request, Transferencia $transferencia, User $usuario, stdClass $erro)
    {
//        return $request->all();

        $request->validate([
            'valor' => 'required',
            'agencia' => 'required|min:4|max:4',
            'conta' => 'required|min:6|max:6',
            'token_transacao' => 'required|min:4|max:4',
            'password' => 'required'
        ]);

        // verificando se a senha digitada está correta

//        $password = Hash::make($request->senha);

//        dd( (auth()->user()->password) ."      ".$password  );
//
//        $user_pagador_validate_password = User::where('password', $password)->select('password')->first();
//
//        dd($user_pagador_validate_password);

        if (! Hash::check($request->password, auth()->user()->password)) {
            Session::flash('tipo', 'warning');
            Session::flash('erro', 'Senha inválida');

            return back()->withInput();
        }


//        return $user_pagador_validate_password;

        $user_receptor_id = User::where('cpf', $request->cpf)->select('id')->first();

//       return $user_receptor_id;

        // dd($request->all());

        // Caso o CPF não exista
        if (!$user_receptor_id) {

            $erro->tipo = 'warning';
            $erro->mensagem = 'CPF inválido';

            Session::flash('tipo', $erro->tipo);
            Session::flash('erro', $erro->mensagem);

            return back()->with('erro', $erro);



            // return 2;

        }

        //esse if é o certo
        // if (! $this->validaTempoToken($request->token)) {
        //     $erro->tipo = 'warning';
        //     $erro->mensagem = 'O Token digitado expirou, ou é inválido para essa operação';
        //     return back()->with(compact('erro', 'input'));

        // }

        // Caso o usuário não pussua saldo para a tranferencia
        if ($usuario->saldo() < $request->valor) {

            Session::flash('tipo', 'danger');
            Session::flash('erro', 'Saldo insufiente');

            return back();

            // return 3;




        }

        // Realizar a transferencia
        $transferencia->valor = $request->valor;
        $transferencia->user_pagador_id = auth()->user()->id;
        $transferencia->user_receptor_id = $user_receptor_id->id;
        // Salvar no banco de dados

        
        $transferencia->save();
       

        if (!$transferencia) {
            Session::flash('erro', 'Transferencia nao realizada, por favor tente novamente mais tarde');
            Session::flash('tipo', 'danger');
            return back();
        }

        // Voltar para a rota home

           return view('home')->with([
               Session::flash('success', 'Tranferencia realizada com sucesso!'),
                Session::flash('tipo', 'danger')
           ]);
        // return 4;

    }

    // public function validaTempoToken($token)
    // {
    //     date_default_timezone_set('America/Sao_Paulo');

    //     $tokenData = Carbon::parse($token->created_at)->format('Y-m-d H:i:s');
    //     $dataAtual = Carbon::now();

    //     $diferenca = $dataAtual->diffInSeconds($tokenData);

    //     // dd($diferenca);
    //     // o horario no seu servidor não tá o mesmo da maquina, depois da uma olhada nisso

    //     if ( $diferenca >= 60 && $token->tipo != 1)
    //         return false;
    //     else
    //         return true;

    // }

        public function validaTempoToken($token)
    {
        date_default_timezone_set('America/Sao_Paulo');

        $tokenData = Carbon::parse($token->created_at)->format('Y-m-d H:i:s');
        $dataAtual = Carbon::now();

        //dd($dataAtual->diffInSeconds($tokenData));

        $diferenca = $dataAtual->diffInSeconds($tokenData);

        if ( intval($diferenca) >= 60 ) 
            return true;
        else
            return false;

    }

}
