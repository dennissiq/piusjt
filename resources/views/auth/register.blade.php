@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="col-lg-6 sk-boxShadow m-auto px-0">
                 <h2 class="sk-boxShadow-dark-gray alfaSlab text-center px-2 py-4">7 Keys - {{ __('Cadastrar-se') }}</h2>
                 @if( session('erro') )
                 <div class="alert alert-{{ session('tipo') }} alert-dismissible fade show" role="alert">
                     {{ session('erro') }}
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                     </button>
                 </div>
                 @endif

                 @if ($errors->any())
                      <div class="alert alert-danger">
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif

                    <form method="POST" action="{{ route('post-register') }}">
                        @csrf

                                                <div class="form-group row">
                          <div class="col-md-2">
                          <label for="nome" class="  mx-4 col-form-label text-md-right">{{ __('Nome') }}</label>

                        </div>
                            <div class="col-md-8">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} sk-tbox" name="name" value="{{ old('name') }}"  autofocus>
                                <div class="msg" id="msg-nome">
                                    * Ops, este campo é obrigatório.
                                </div>
                                <!-- @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif -->
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-2">
                            <label for="email" class="  mx-4 col-form-label text-md-right">{{ __('E-mail') }}</label>
</div>
                            <div class="col-md-8">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} sk-tbox" name="email" value="{{ old('email') }}">
                                <div class="msg" id="msg-email">
                                    * Este campo é obrigatório.
                                </div>
                                <!-- @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif -->
                            </div>
                        </div>

                        <div class="form-group row">
                          <div class="col-md-2">
                          <label for="agencia" class="  mx-4 col-form-label text-md-right">Agência</label>
                        </div>
                            <div class="col-md-8">
                                <input id="agencia" type="number" class="form-control sk-tbox" name="agencia" value="" >
                                <div class="msg" id="msg-agencia">
                                    * Ops, este campo é obrigatório. A agência deve ter 4 dígitos.
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                          <div class="col-md-2">
                            <label for="conta" class="  mx-4 col-form-label text-md-right">Conta</label>
                          </div>
                            <div class="col-md-8">
                                <input id="conta" type="text" class="form-control sk-tbox" name="conta" value="" >
                                <div class="msg" id="msg-conta">
                                    *Ops, este campo é obrigatório. A conta deve ter 5 dígitos.
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                          <div class="col-md-2">
                          <label for="email" class="  mx-4 col-form-label text-md-right">CPF</label>
                        </div>
                            <div class="col-md-8">
                                <input id="cpf" type="text" class="form-control sk-tbox" name="cpf" value="" >
                                <div class="msg" id="msg-cpf">
                                    * este campo é obrigatório e  deve ter 11 digítos.
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                          <div class="col-md-2">
                          <label for="email" class="  mx-4 col-form-label text-md-right">{{ __('Senha') }}</label>
                        </div>
                            <div class="col-md-8">
                                <input id="password" type="password"  class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} sk-tbox" name="password" >
                                <div class="msg" id="msg-password">
                                    * Este campo tem que ter de 6 à 8 dígitos .
                                </div>
                                <!-- @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif -->
                            </div>
                        </div>

                        <div class="form-group row">
                          <div class="col-md-2">
                          <label for="confirmarSenha" class="  col-form-label text-md-right">{{ __('Confirmar senha') }}</label>
                        </div>
                            <div class="col-md-8">
                                <input id="password-confirm" type="password" class="form-control sk-tbox" name="password_confirmation" >
                                <div class="msg" id="msg-confirmPassword">
                                    * Este campo tem que ter de 6 à 8 dígitos .
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mx-5 ">
                            <div class="col-md-10 mx-5">
                                <button type="submit" class="btn btn sk-btn-b btn-lg w-100  my-2">
                                    {{ __('Cadastrar') }}
                                </button>
                            </div>
                        </div>
                    </form>

            </div>
        </div>
    </div>
</div>
@endsection
