@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div class="row ">
    <div class="col-md-12">
            <div class="col-lg-6 sk-boxShadow m-auto px-0">
               <h2 class="sk-boxShadow-dark-gray alfaSlab text-center px-2 py-4">7Keys</h2>
                <div class="col-md-12">
                @if(! empty(session('tipo')))
                <div class="alert alert-<?= session('tipo') ?? '' ?> alert-dismissible fade show" role="alert">
                    <?= session('mensagem') ?? '' ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
            </div>

{{--                <!-- <div class="card-header">{{ __('Login') }}</div> -->--}}

                <div class=" sk-boxShadow m-auto px-0 " >
                    <form method="POST" action="{{ route('autenticar') }}">
                        @csrf

                        <div class="orm-group row mx-2 ">

                            <div class="col-md-6">
                                <span class="input-group-addon"><i class="fas fa-university fa-md light-grayF iconPad"></i></span>
                                <label for="">Agência</label>
                                <input id="agencia" type="agencia" class="form-control sk-tbox" name="agencia" value="{{ session('input')['agencia'] }}{{ old('agencia') }}" required autofocus>
                                <div class="msg" id="msg-agencia">
                                    * Ops, este campo é obrigatório. A agência deve ter 4 dígitos.
                                </div>
                            </div>

                            <div class="col-md-6">
                                <span class="input-group-addon"><i class="fas fa-book fa-md light-grayF iconPad"></i></span>
                                <label for="">Conta</label>
                                <input id="conta" type="conta" class="form-control sk-tbox" name="conta" value="{{ session('input')['conta'] }}{{ old('conta') }}" required autofocus>
                                <div class="msg" id="msg-conta">
                                    *Ops, este campo é obrigatório. A conta deve ter 6 dígitos.
                                </div>
                            </div>
                        </div>

                        <div class="row mx-2 my-2">

                            <div class="col-md-12">
                               <span class="input-group-addon"><i class="fas fa-lock fa-md light-grayF iconPad" aria-hidden="true"></i></span>
                                <label for="">Senha</label>
                                <input id="password" type="password" class="form-control sk-tbox" name="password" value="{{ session('input')['password'] }}{{ old('password') }}" required>
                                <div class="msg" id="msg-password">
                                    * Este campo tem que ter de 6 à 8 dígitos .
                                </div>
                            </div>

                        </div>

                        <div class="form-group row mx-2">
                            <div class="col-md-12">
                                <div class="checkbox">
                                  <span class="input-group-addon sk-tbox"><i class="fas fa-key fa-md light-grayF iconPad" aria-hidden="true"></i></span>
                                    <label for="">Token</label>
                                    <input id="token" type="token" class="form-control" name="token" value="{{ session('input')['token'] }}{{ old('token') }}" >
                                    <div class="msg" id="msg-token">
                                        * Por favor, digite um token válido.
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- <div class="form-group row mx-2">
                          <div class="col-md-12">
                            <div class="g-recaptcha" data-sitekey="6Lfmmk4UAAAAACY6fqavfAgIUASB3LYcMW3Wps4J"></div>
                          </div>
                        </div> -->

                        <div class="form-group row mx-2">
                            <div class="col-md-12">
                                <button type="submit" class="btn sk-btn-b btn-lg w-100 my-3" >
                                    Acessar
                                </button>

                                {{--<a class="btn btn-link" href="{{ route('password.request') }}">--}}
                                    {{--{{ __('Forgot Your Password?') }}--}}
                                {{--</a>--}}
                            </div>
                        </div>
                    </form>


        </div>
    </div>
</div>

<!-- <script src='https://www.google.com/recaptcha/api.js'></script> -->

@endsection
