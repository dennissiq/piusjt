<!-- Modal Confirmação de tranferencia -->

<div class="modal fade" id="mdTransferencia">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Confirmação da tranferência</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <form action="{{ route('TransferenciaStore') }}" id="formularioTransferencia"  method="post">
        <input type="hidden" name="cpf_input" id="cpf_input" >
        <input type="hidden" name="valor_input" id="valor_input" >
          <!-- <input type="hidden" name="senha_input" id="senha_input" > -->

          @csrf
      <div class="modal-body">
        <div class="col-sm-12"><label>Nome: </label><b><label id="conf_nome"></label></b></div>
        <div class="col-sm-12"><label>Agência: </label><b><label id="conf_agencia"></label></b></div>
        <div class="col-sm-12"><label>Conta: </label><b><label id="conf_conta"></label></b></div>
        <div class="col-sm-12 "><label>CPF: </label><b><label id="conf_cpf"></label></b></div>
        <div class="col-sm-12"><label>Valor: </label><b><label id="conf_valor"></label></b></div>
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <div class="col-sm-12">
          <a href="">
            <button  class="btn sk-btn-b-back ">voltar</button>
          </a>
        <!-- <button  type="button"  class="btn sk-btn-b" data-toggle="modal" data-target="#mdDetalheTransferencia">Confimar</button> -->
          <button  class="btn sk-btn-b" id="confirmar" >Confimar</button>

        </div>
      </div>
    </form>
    </div>
  </div>
</div>
