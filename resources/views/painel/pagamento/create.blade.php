@extends('painel.app')

@section('conteudo')

<div class="container-fluid">
  <div class="row">
    <div class="col-lg-12 ">
      <h2 class="sk-title  alfaSlab mx-5 ">Pagamento</h2>
    </div>

        <!-- <form action="{{ route('TransferenciaStore') }}" id="formularioTransferencia" method="post"> -->
    <div class=" col-lg-6 sk-boxShadow m-auto px-0">

      <h2 class="sk-btn-c px-2" >Contas </h2>


      <hr>
      <!-- @csrf -->
      <div class="form-group">
        <div class="form-row px-2">
        <div class="col-md-12">
                     @if(! empty(session('tipo')))
                <div class="alert alert-<?= session('tipo') ?? '' ?> alert-dismissible fade show" role="alert">
                    <?= session('erro') ?? '' ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif

                </div>
                <form id="formSubmitPagamento" class="p-3" action="{{ route('PagamentoStore') }}" method="post"

                <div id="errors-ajax">

                    </div>

                    @if ($errors->has('agencia'))>
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>Agência inválida!</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif

                    @if ($errors->has('conta'))
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Conta inválida!</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    @if ($errors->has('valor'))
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Valor Inválido!</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button> 
                        </div>
                    @endif

                    @if ($errors->has('token'))
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Token inválido!</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    @csrf

          <div class="col-md-12">
            <label for="valor" >Valor a ser debitado</label>
            <input name="valor" id="valor" type="text" class="form-control sk-tbox">
            <div class="msg" id="msg-valor">
								* Este campo é obrigatório.
						</div>
          </div>
          <div class="col-md-12">
            <label for="senha" >Senha</label>
            <input name="password" id="senha" type="password" class="form-control sk-tbox">
            <div class="msg" id="msg-password">
                * Este campo deve ter de 6 à 8 dígitos .
            </div>
          </div>
          <div class="col-md-12">
            <label for="token" >Token</label>
            <input name="token_transacao" id="token" type="text" class="form-control sk-tbox" maxlength="4">
            <div class="msg" id="msg-tokenTransacao">
                * Este campo é obrigatório .
            </div>
          </div>
          <div class="col-md-12 my-4">
            <button type="button" id="irPagamento" class="btn sk-btn-b btn-lg w-100"  data-toggle="modal">Ir <i class="fa fa-chevron-right fa-sm"></i></button>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>


@include('painel.pagamento.modal.confirmacaoPagamento')
@endsection

@section('linksJavaScript')
    <script type="text/javascript" src="/js/pagamento.js"></script>
  <!-- <script type="text/javascript" src="/js/conf_pagamento.js"></script> -->
  <script type="text/javascript" src="/js/validaForm.js"></script>



@endsection
