@extends('painel.app')

@section('conteudo')

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 ">
                <h2 class="sk-title  alfaSlab mx-5 ">Transferência bancária</h2>
            </div>

            <div class=" col-lg-6 sk-boxShadow m-auto px-0">

                <h2 class="sk-btn-c px-2">Transferência</h2>


                <hr>



                <div class="col-md-12">
                     @if(! empty(session('tipo')))
                <div class="alert alert-<?= session('tipo') ?? '' ?> alert-dismissible fade show" role="alert">
                    <?= session('erro') ?? '' ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif

                </div>

                <form id="formSubmitTransferencia" class="p-3" action="{{ route('autenticarTransferencia') }}" method="post"
                      id="formTransferencia">

                    <div id="errors-ajax">

                    </div>

                    @if ($errors->has('agencia'))>
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>Agência inválida!</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif

                    @if ($errors->has('conta'))
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Conta inválida!</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    @if ($errors->has('valor'))
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Valor Inválido!</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button> 
                        </div>
                    @endif

                    @if ($errors->has('token'))
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Token inválido!</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    @csrf
                    <div class="form-group">
                        <div class="form-row px-2">

                            <div class="col-md-12">
                                {{--<label for="cpf">CPF</label>--}}
                                <input name="cpf" id="cpf" type="hidden" class="form-control sk-tbox"
                                       value="{{ session('input')['cpf'] }}">
                                {{--<div class="msg" id="msg-cpf">--}}
                                {{--* Ops, o campo cpf deve ter 11 digítos.--}}
                                {{--</div>--}}
                            </div>
                            <div class="col-md-6">
                                <label for="agencia">Agência</label>
                                <input name="agencia" id="agencia" class="sk-tbox form-control" type="text"
                                       class="form-control" value="{{ session('input')['agencia'] }}{{ old('agencia') }}">
                                <div class="msg" id="msg-agencia">
                                    * Ops, o campo agência deve ter 4 digítos.
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="conta">Conta</label>
                                <input name="conta" id="conta" type="text" class="form-control sk-tbox"
                                       value="{{ session('input')['conta'] }}{{ old('conta') }}">
                                <div class="msg" id="msg-conta">
                                    * Ops, o campo conta deve ter 6 digítos.
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label for="valor">Valor a ser transferido</label>
                                {{--<input type="number" min="1" step="any" />--}}
                                <input value="{{ old('valor') }}" name="valor" min="1" step="any"  id="valor" type="number" class="form-control sk-tbox"
                                       autocomplete="off">
                                <div class="msg" id="msg-valor">
                                    * Este campo é obrigatório.
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label for="senha">Senha</label>
                                <input name="password" id="senha" type="password" class="form-control sk-tbox"
                                       value="{{ session('input')['password'] }}{{ old('senha') }}">
                                <div class="msg" id="msg-password">
                                    * Este campo deve ter de 6 à 8 dígitos .
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label for="token">Token</label>
                                <input name="token_transacao" id="token_transacao" type="number" class="form-control sk-tbox" maxlength="4">
                                <div class="msg" id="msg-tokenTransacao">
                                    * Este campo é obrigatório .
                                </div>
                            </div>
                            <div class="col-md-12 my-4">

                                <button type="button" id="ir" class="btn sk-btn-b btn-lg w-100" data-toggle="modal">Ir
                                    <i class="fa fa-chevron-right fa-sm"></i></button>
                                
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- data-target="#mdTransferencia" data-toggle="modal" -->
    @include('painel.transferencia.modal.confirmacaoTransferencia')
@endsection

@section('linksJavaScript')
    <script type="text/javascript" src="{{ asset('js/transferencia.js') }}"></script>
    <!-- <script type="text/javascript" src="/js/validaForm.js"></script> -->
    {{--  <script type="text/javascript" src="{{ asset('js/conf_transferencia.js') }}"></script>--}}
@endsection
