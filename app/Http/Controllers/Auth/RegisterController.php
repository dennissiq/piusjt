<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    protected function store(Request $request, User $usuario, stdClass $erro)
    {
      dd($request);
        // $input = $request->all();
        // dd($input);
        $request->validate([
            'nome' => 'required|min:2|max:100',
            'agencia' => 'required|min:4|max:4',
            'conta' => 'required|min:6|max:6',
            'cpf' => 'required|min:11|max:11',
            'password' => 'required|min:6|max:8',
            'password_confirmation' => 'required|min:6|max:8'
        ]);
        $password = Hash::make($request->senha);

        // Cadastrar usuário
        $usuario->name            = $request->name   ;
        $usuario->agencia         = $request->agencia;
        $usuario->conta           = $request->conta;
        $usuario->cpf             = $request->cpf;
        $usuario->password        = $password;
        // Salvar no banco de dados

        $usuario->save();

        return view('auth.login')->with([
            'message' => 'cadastro efetuado com sucesso!',
             'tipo'  => 'success'
        ]);

      // return  dd($user);
                 // return view('auth.login');
    }

//   protected function create(array $data)
//     {
//         return User::create([
//             'name' => $data['name'],
//             'email' => $data['email'],
//             'agencia' => $data['agencia'],
//             'conta' => $data['conta'],
//             'password' => Hash::make($data['password']),
//             'cpf' => $data['cpf']
//         ]);
//     }
}
