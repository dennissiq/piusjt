<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransferenciaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cpf' => "required|min:11|max:11",
            'agencia' => "required|min:4|max:4",
            'conta' => "required|min:5|max:5",
            'password' => "required|min:6|max:10"

        ];
}
      public function messages(){
        return
        'cpf.required' =>'O campo deve ser preenchido',
        'agencia.required' => 'O campo deve ser preenchido',
        'conta.required' => 'O campo deve ser preenchido',
        'password.required' => 'O campo deve ser preenchido',


      }
