var ajax = true;
function validaForm(obj, de, ate, msg){
  obj.blur(function(){
    if(obj.val().length < de || obj.val().length > ate){
    obj.css("border-color", "#ca5141");
    msg.css("display", "block");
    ajax = false;

  }else{
    obj.css("border-color", "#89bf55");
    msg.css("display", "none");
    ajax = true;
  }
});
}



$(function(){

  var form = $("#formTransferencia");
  var name = $("input[name=name]");
  var email = $("input[name=email]");
  var cpf = $("input[name=cpf]");
  var agencia = $("input[name=agencia]");
  var conta = $("input[name=conta]");
  var senha = $("input[name=password]");
  var confirmaSenha = $("input[name=password_confirmation");
  var token = $("input[name=token");
  var tokenTransacao = $("input[name=token_transacao");
  var valor = $("input[name=valor]");

  validaForm(name, 2, 100, $("#msg-nome"));
  validaForm(email,10 , 30, $("#msg-email"));
  validaForm(cpf, 11, 11, $("#msg-cpf"));
  validaForm(agencia, 4, 4, $("#msg-agencia"));
  validaForm(conta, 6, 6, $("#msg-conta"));
  validaForm(senha, 6, 8, $("#msg-password"));
  validaForm(confirmaSenha, 6, 8, $("#msg-confirmPassword"));
  validaForm(token, 6, 6, $("#msg-token"));
  validaForm(tokenTransacao, 4, 4, $("#msg-tokenTransacao"));
  validaForm(valor, 1, 20, $("#msg-valor"));


//   form.submit(function(event){
//     event.preventDefault();
//
//     //console.log(nome.length);
//     // if(ajax == true){
//     //   $.ajax({
//     //     headers: {
//     //           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//     //           },
//     //     url: "transferencia/auth",
//     //     data: form.serializeArray(),
//     //     type: 'POST',
//     //     sucess:function(data){
//     //       form.load("transferencia/store");
//     //     }
//     //       });
//     //     }
// });

});
