<?php

Route::get('/', function () {
    return redirect('internetbanking');
});
  Route::get('novo-usuario', 'RegisterController@getRegister')->name('get-register');
  Route::post('novo-usuario', 'RegisterController@postRegister')->name('post-register');

    Route::get('login', 'LoginController@create')->name('login');

  Auth::routes();
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Este arquivo conterá todas as informações e rotas do sistema
| Depois de logado o usuario permanecerá dentro do diretorio /internetbank
|

*/

Route::group(['prefix' => 'internetbanking'], function(){

    Route::post('/logar', 'Auth\LoginController@authenticate')->name('autenticar');

    Route::get('/', 'HomeController@index')->name('home');

    Route::group(['prefix' => 'transferencia'], function() {

        Route::get('', 'TransferenciaController@create')->name('TransferenciaCreate');
        Route::post('/auth', 'TransferenciaController@authenticate')->name('autenticarTransferencia');

        Route::post('/store', 'TransferenciaController@store')->name('TransferenciaStore');

        Route::get('/pega-infos-receptor', 'TransferenciaController@verificaReceptor')->name('pega_infos_receptor');

        Route::get('/confirmacao-transferencia', 'TransferenciaController@confirmacaoTransferencia')->name('confirmacaoTransferencia');



    });

    Route::group(['prefix' => 'extrato'], function() {

        Route::get('/', 'ExtratoController@extratos')->name('ExtratoIndex');

    });

    Route::group(['prefix' => 'pagamento'], function() {

        Route::get('/', 'PagamentoController@create')->name('PagamentoCreate');
          Route::post('/store', 'PagamentoController@store')->name('PagamentoStore');

    });

});
