<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Auth;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use stdClass;
use App\User;
use App\TokenAccess;
use Illuminate\Support\Facades\Session;
// use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{


    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function create()
    {
//        $deposito = new UserController();
//        $saldo = $deposito->saldo();
//        return $saldo;
        return view('auth.login');
    }


    public function authenticate(Request $request, stdClass $erro)
    {
        $token = TokenAccess::where('token', $request->token)->first();

        $input = $request->all();

        //   $message = [
        //     'agencia.required' => 'O campo agência é necessario',
        //     'agencia.size'     => 'O Campo agência deve conter 4 caracteres',
        //
        //     'conta.required'   => 'O campo conta é necessario',
        //     'conta.size'       => 'O Campo conta deve conter 6 caracteres',
        //
        //
        //     'password.required' => 'O campo senha é necessario',
        //     'password.min'      => 'O campo senha deve conter no mínimo 4 caracteres',
        //     'password.max'      => 'O campo senha deve conter no maxímo 6 caracteres',
        //
        //     'token.required'    => 'O campo token é necessário',
        //     'token.size'        => 'O campo token deve conter 6 caracteres'
        //
        //   ];
        //
        //   $rules = [
        //   'agencia' => 'required|size:4|',
        //   'conta'   =>  'required|size:6',
        //   'password'=>  'required|min:6|max:8',
        //   'token'   =>  'required|size:6'
        // ];
        //
        // $validator = Validator::make($input, $rules, $message);
        //
        // $validator->validate();

        //if (!$token ) {
        //     $erro->tipo = 'danger';
        //     $erro->erro = 'O Token digitado é inválido';
        //
        //     return back()->with(compact('erro', 'input'));
        //$request->session()->flash('tipo', 'danger');
        //$request->session()->flash('erro', 'O Token digitado é inválido');
        //return back();
        //}


        if ( empty($token) ) {

            $request->session()->flash('tipo', 'danger');
            $request->session()->flash('mensagem', 'O Token digitado é inválido');

            return back()->withInput(); 
        }

        if ($token->estado == 0) {
            $request->session()->flash('tipo', 'danger');
            $request->session()->flash('mensagem', 'O Token digitado já foi utilizado');

            return back()->withInput();
        }


       //dd($this->validaTempoToken($token));
        
        if($token->tipo != 2) {
            // $request->session()->flash('tipo', 'danger');
            // $request->session()->flash('mensagem', 'O Token digitado não é válido para este tipo de operação');

            // return view('auth.login')->withInput();
            // return back()->withInput();
            return back()->withInput()->with([
          Session::flash('mensagem',  'O Token digitado não é válido para este tipo de operação'),
          Session::flash('tipo', 'danger')
        // return 1;
      ]);

        }

        // testa agora

        if ($this->validaTempoToken($token)) {
            $request->session()->flash('tipo', 'danger');
            $request->session()->flash('mensagem', 'O Token digitado expirou');
            
            return back()->withInput();

        }

        $request->validate([
            'token' => 'required|min:6|max:6'
        ]);

        // metodo para autenticar o usuário
        $autenticaUsuario = Auth::attempt(
            [
                'agencia' => $request->agencia,
                'conta' => $request->conta,
                'password' => $request->password

            ]);

        if (!$autenticaUsuario) {

          $request->session()->flash('tipo', 'warning');
          $request->session()->flash('erro', 'Verifique se os dados digitados estão corretos');
          return back();
            // $erro->tipo = 'warning';
            // $erro->erro = 'Verifique se os dados digitados estão corretos';
            // return back()->with('erro', $erro);
        }

       $token->estado = 0;
       $token->save();

        return redirect()->route('home');
    }

    public function validaTempoToken($token)
    {
        date_default_timezone_set('America/Sao_Paulo');

        $tokenData = Carbon::parse($token->created_at)->format('Y-m-d H:i:s');
        $dataAtual = Carbon::now();

        //dd($dataAtual->diffInSeconds($tokenData));

        $diferenca = $dataAtual->diffInSeconds($tokenData);

        if ( intval($diferenca) >= 60 ) 
            return true;
        else
            return false;

    }
}
