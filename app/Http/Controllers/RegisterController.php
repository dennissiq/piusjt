<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Session;

// use Illuminate\Validation\Validator;

class RegisterController extends Controller
{
  private $user;

  public function __construct(User $user)
  {
    $this->user = $user;
  }
  protected $redirectTo = 'login';

  public function getRegister()
  {
      return view('auth.register');
  }

  public function postRegister(Request $request)
  {

      $message = [
        'name.required' => 'O campo nome é necessario',
        'name.min' => 'O campo nome deve conter no minímo 2 caracteres',
        'name.max' => 'O campo nome deve conter no maxímo 100 caracteres',

        'agencia.required' => 'O campo agência é necessario',
        'agencia.size' => 'O Campo agência deve conter 4 caracteres',
        // 'agencia.max' => 'O Campo agência deve conter 4 caracteres',

        'conta.required' => 'O campo conta é necessario',
        'conta.size' => 'O Campo conta deve conter 6 caracteres',
        'conta.unique' => 'Esta conta já está cadastrada',

        'cpf.required' => 'O campo cpf é necessario',
        'cpf.unique' => 'O cpf já está cadastrado',
        'cpf.min' => 'O campo cpf deve conter 11 caracteres',

        'email.required' => 'O campo email é necessario',
        'email.unique' => 'O email já está cadastrado',
        'email.max' => 'O campo email deve conter nomáximo 255 caracteres',

        'password.required' => 'O campo senha é necessario',
        'password.min' => 'O campo senha deve conter no mínimo 4 caracteres',
        'password.max' => 'O campo senha deve conter no maxímo 6 caracteres',

        'password_confirmation.required' => 'O campo confimar senha é necessario',
        'password_confirmation.min' => 'O campo confimar senha deve conter no mínimo 4 caracteres',
        'password_confirmation.max' => 'O campo confimar senha deve conter no maxímo 6 caracteres',
        'password_confirmation.same' => 'O campo confimar senha não corresponde ao campo senha'
      ];


      $data = $request->all();

      $rules = [
          'name' => 'required|min:2|max:100',
          'agencia' => 'required|size:4',
          'email' => 'required|string|email|max:255|unique:users',
          'conta' => 'required|size:6|unique:users',
          'cpf' => 'required|min:11|max:11|unique:users',
          'password' => 'required|min:6|max:8',
          'password_confirmation' => 'required|min:6|max:8|same:password'
      ];

      $validator = Validator::make($data, $rules, $message);

      $validator->validate();

      $password = Hash::make($request->password);

      // Cadastrar usuário
      $this->user->name            = $request->name;
      $this->user->agencia         = $request->agencia;
      $this->user->conta           = $request->conta;
      $this->user->email           = $request->email;
      $this->user->cpf             = $request->cpf;
      $this->user->password        = $password;
      // Salvar no banco de dados

      if (! $this->user->save() ) {
        $request->session()->flash('tipo', 'error');
        $request->session()->flash('erro', 'Erro ao cadastrar usuário');
        return redirect()->back();
      }

      return redirect()->route('login')->with([
          Session::flash('mensagem',  'cadastro efetuado com sucesso!'),
          Session::flash('tipo', 'success')
      ]);


  }
}
