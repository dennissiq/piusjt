<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Pagamento;
use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\TokenAccess;
use Illuminate\Support\Facades\Hash;



class PagamentoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        return view('painel.pagamento.create');
    }

  //   public function authenticate(Request $request, stdClass $erro)
  //   {
  //       $token = TokenAccess::where('token', $request->token)->first();
  //
  //       $data = $request->all();
  //
  //         $message = [
  //           'valor.required' => 'O campo avlor é necessario',
  //
  //           'password.required' => 'O campo senha é necessario',
  //           'password.min'      => 'O campo senha deve conter no mínimo 4 caracteres',
  //           'password.max'      => 'O campo senha deve conter no maxímo 6 caracteres',
  //
  //           'token.required'    => 'O campo token é necessário',
  //           'token.size'        => 'O campo token deve conter 6 caracteres'
  //
  //         ];
  //
  //         $rules = [
  //         'valor' => 'required',
  //         'password'=>  'required|min:6|max:8',
  //         'token'   =>  'required|size:6'
  //       ];
  //
  //       $validator = Validator::make($data, $rules, $message);
  //
  //       $validator->validate();
  //
  //       if (! $token) {
  //       //     $erro->tipo = 'danger';
  //       //     $erro->erro = 'O Token digitado é inválido';
  //       //
  //       //     return back()->with(compact('erro', 'input'));
  //       $request->session()->flash('tipo', 'danger');
  //       $request->session()->flash('erro', 'O Token digitado é inválido');
  //       return back();
  //       }
  //
  //       if ($token->estado == 0) {
  //           // $erro->tipo = 'danger';
  //           // $erro->erro = 'O Token digitado já foi utilizado';
  //           // return back()->with(compact('erro', 'input'));
  //           $request->session()->flash('tipo', 'danger');
  //           $request->session()->flash('erro', 'O Token digitado já foi utilizado');
  //           return back();
  //       }
  //
  //       if ($this->validaTempoToken($token)) { //
  //           // $erro->tipo = 'warning';
  //           // $erro->erro = 'O Token digitado expirou, gere um novo Token e tente novamente';
  //           // return back()->with(compact('erro', 'input'));
  //           $request->session()->flash('tipo', 'warning');
  //           $request->session()->flash('erro', 'O Token digitado expirou, gere um novo Token e tente novamente');
  //           return back();
  //       }
  //
  //       // metodo para autenticar o usuário
  //       $autenticaUsuario = Auth::attempt(
  //           [
  //               'agencia' => $request->agencia,
  //               'conta' => $request->conta,
  //               'password' => $request->password
  //           ]);
  //
  //       if (!$autenticaUsuario) {
  //
  //         $request->session()->flash('tipo', 'warning');
  //         $request->session()->flash('erro', 'Verifique se os dados digitados estão corretos');
  //         return back();
  //           // $erro->tipo = 'warning';
  //           // $erro->erro = 'Verifique se os dados digitados estão corretos';
  //           // return back()->with('erro', $erro);
  //       }
  //
  // //        $token->estado = 0;
  // //        $token->save();
  //
  //       }
  //
  //   public function validaTempoToken($token)
  //   {
  //       date_default_timezone_set('America/Sao_Paulo');
  //
  //       $tokenData = Carbon::parse($token->created_at)->format('Y-m-d H:i:s');
  //       $dataAtual = Carbon::now();
  //       // pqp essa merda tá MINUTOS eu botei em minutos pq preicsava testar
  //
  //       $diferenca = $dataAtual->diffInSeconds($tokenData);
  //
  //       // dd($diferenca);
  //       // o horario no seu servidor não tá o mesmo da maquina, depois da uma olhada nisso
  //
  //       if ( $diferenca >= 60)
  //           return true;
  //       else
  //           return false;
  //
  //   }


    public function store(Request $request, Pagamento $pagamento, User $usuario)
    {

         $request->validate([
            'valor' => 'required',
            'token' => 'required|min:4|max:4',
            'password' => 'required'
        ]);

        $autenticaUsuario = Auth::attempt(
            [
                
                'password' => $request->password
            ]);

        if (!$autenticaUsuario) {
            $erro->tipo = 'warning';
            $erro->mensagem = 'Verifique se os dados digitados estão corretos';
            return back()->with('erro', $erro);
        }

            $password_user = Hash::check($request->password, auth()->user()->password);
            if (! $password_user) {
            // Session::flash('tipo', 'warning');
            // Session::flash('erro', 'Senha inválida');

            // return response()->json(['status' => 0, 'erro' => 'Dados inválidos']);

             return back()->with([
            Session::flash('erro',  'senha invalida!'),
            Session::flash('tipo', 'success')
          ]);
        }else{
             return response()->json($password_user);
        }
        

        // if(! $user_pagador_validate_password ) {
        //     Session::flash('tipo', 'warning');
        //     Session::flash('erro', 'Senha inválida');

        //     return back()->response()->json(['status' => 0]);
        // }



        // $password = Hash::make($request->password);

        // $user_pagador_validate_password = User::where('password', $password)->select('password')->first();

        // if(! $user_pagador_validate_password ) {
        //     Session::flash('tipo', 'warning');
        //     Session::flash('erro', 'Senha inválida');

        //     return back()->response()->json(['status' => 0]);
        // }
         // Caso o usuário não pussua saldo para a tranferencia
        if( $usuario->saldo() < $request->valor) {
            // return back()->with('message', 'Saldo insuficiente');

            $request->session()->flash('tipo', 'warning');
            $request->session()->flash('erro', 'Saldo insuficiente');
            return back();
        }

        // Realizar a transferencia
        $pagamento->valor                   = $request->valor;
        $pagamento->user_id                 = auth()->user()->id;

        // Salvar no banco de dados
        $pagamento->save();

        // Voltar para a rota home
        return redirect()->route('home')->with([
            Session::flash('erro',  'Pagamento efetuado com sucesso!'),
            Session::flash('tipo', 'success')
          ]);

    }

}
