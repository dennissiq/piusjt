$("#ir").on("click", function () {

// console.log("entrou no IR");
    $.ajax({
        type: 'get',
        url: 'transferencia/pega-infos-receptor', // deverá ser criada essa rota, e um metodo em algum controller que vai retornar o nome do usuario pelo cpf digitado
        data: {
            agencia: $("#agencia").val(),
            conta: $("#conta").val(),
            senha: $("#senha").val()
        },
        dataType: 'json',
        success: function (response) {
            console.log("Resposta da API", response);

            // Usuario com agencia e conta inexistente
            if (response.status == 0) {
                $("#ir").attr("data-target", "");
                // $(".alert-").addClass("alert-danger");
                //  $(".alert-").html(response.message);

                // alert(response.message);
                $("#errors-ajax").html(
                    "<div class='alert alert-warning m-2'>" +
                    "<strong>" + response.message + "</strong>" +
                    "  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n" +
                    "    <span aria-hidden=\"true\">&times;</span>\n" +
                    "  </button>"+
                    "</div>"
                );

            } else {
                // $("#ir").attr("data-target", "#mdTransferencia");
                $('#mdTransferencia').modal();
                $("#cpf").val(response.cpf);
                $("#agencia").val(response.agencia);
                $("#conta").val(response.conta);
                // $("#valor").val();
                $("#nome").val(response.nome);


                $("#conf_conta").html(response.conta);
                $("#conf_agencia").html(response.agencia);
                $("#conf_cpf").html(response.cpf);
                $("#conf_nome").html(response.name);
                $("#conf_valor").html($("#valor").val());
            }

            //   var agencia = $('#agencia').val();
            //    $("#conf_agencia").html(agencia);
            //    var conta = $('#conta').val();
            //     $("#conf_conta").html(conta);
            //     var cpf = $('#cpf').val();
            //      $("#conf_cpf").html(cpf);
            //   // $("#conf_agencia").html(data.agencia);
            //   // $("#conf_conta").html(data.conta);
            //   $("#conf_nome").html(data.name);
            //   // console.log(data.name);
            //   // $("#conf_cpf").html(data.cpf);
            //
            // var valor = $("#valor").val();
            // console.log("valor:", valor);
            // $("#conf_valor").html(valor);
            // console.log("conf_valor:", valor);

            // var cpf = $("#cpf").val();
            // console.log("cpf:", cpf);
            // $("#cpf_valor").html(cpf);
            //   console.log("cpf_valor:", cpf);

            $(function () {
                $("#confirmar").on("click", function (event) {
                    event.preventDefault();
                    $("#formSubmitTransferencia").submit();
                });
            });
        },
        error: function (response) {
            alert("Tivemos problemas em contactar o servidor, tente novamente mais tarde.");
            console.log(response);
        }

    });
});
// }

// bloquear caso informacoes sejam invalidas
