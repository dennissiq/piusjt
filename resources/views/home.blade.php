@extends('painel.app')

@section('conteudo')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="sk-title  alfaSlab mx-5">Home</h2>
            </div>
        </div>

        <div class="row">
          <div class="mx-5 col-md-4">
          @if( session('erro') )
          <div class="alert alert-{{ session('tipo')}} alert-dismissible fade show" role="alert">
              {{ session('erro')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          @endif
      </div>
            <div class="col-lg-12 text-center sk-title">
                <!-- Mensagem para usuário -->
                <h1><label id="idUsuário">{{ auth()->user()->name  }},</label></h1>
                <h2>Por onde começamos?</h2>
            </div>
        </div>
        <div class="mx-5 mt-3">
            <div class="row">
                <!-- Botão para transferência -->
                <div class="col-lg-4">
                    <a href="{{ route('TransferenciaCreate') }}">
                        <button type="button" class="btn btn-block sk-btn-a py-4 mb-2 btn-lg">transferir dinheiro</button>
                    </a>
                </div>
                <!-- Botão para extrato -->
                <div class="col-lg-4">
                    <a href="{{ route('ExtratoIndex') }}">
                        <button type="button" class="btn btn-block sk-btn-a py-4 mb-2 btn-lg">checar extrato</button>
                    </a>
                </div>
                <!-- Botão para pagamento -->
                <div class="col-lg-4">
                    <a href="{{ route('PagamentoCreate') }}">
                        <button type="button" class="btn btn-block sk-btn-a py-4 mb-2 btn-lg">pagar contas</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
